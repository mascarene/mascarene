/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.utils

import scala.util.{Failure, Success, Try}

sealed trait Sigil
case object UserSigil      extends Sigil { override def toString: String = "@" }
case object RoomSigil      extends Sigil { override def toString: String = "!" }
case object EventSigil     extends Sigil { override def toString: String = "$" }
case object RoomAliasSigil extends Sigil { override def toString: String = "#" }

trait MatrixIdentifier {
  def localPart: String
  def domain: String
  def sigil: Sigil
  override def toString: String = s"$sigil$localPart:$domain"
}

case class UserIdentifier(localPart: String, domain: String) extends MatrixIdentifier {
  val sigil: Sigil     = UserSigil
  def isValid: Boolean = UserIdentifierUtils.parse(this.toString).isSuccess
}

case class RoomIdentifier(localPart: String, domain: String) extends MatrixIdentifier {
  val sigil: Sigil     = RoomSigil
  def isValid: Boolean = RoomIdentifierUtils.parse(this.toString).isSuccess
}

case class RoomAliasIdentifier(localPart: String, domain: String) extends MatrixIdentifier {
  val sigil: Sigil     = RoomAliasSigil
  def isValid: Boolean = RoomAliasIdentifierUtils.parse(this.toString).isSuccess
}

object UserIdentifierUtils {
  private val userIdPattern = """^@([\p{Lower}\d\._=-]*):(.+)""".r
  def parse(identifier: String): Try[UserIdentifier] = {
    identifier match {
      case userIdPattern(localName, domain) => Success(UserIdentifier(localName, domain))
      case _                                => Failure(new IllegalArgumentException(s"'$identifier' doesn't match matrix User ID format"))
    }
  }
  def build(localPart: String, domainName: String): Try[UserIdentifier] = {
    val built = UserIdentifier(localPart, domainName)
    if (built.isValid)
      Success(built)
    else
      Failure(new IllegalAccessException(s"'$built' doesn't match matrix User ID format"))
  }

  def generate(domainName: String): UserIdentifier = UserIdentifier(Codecs.genUserId(), domainName)
}

object RoomIdentifierUtils {
  private val roomIdPattern = """^!([\p{Alnum}\._=-]*):(.+)""".r
  def parse(identifier: String): Try[RoomIdentifier] = {
    identifier match {
      case roomIdPattern(localName, domain) => Success(RoomIdentifier(localName, domain))
      case _                                => Failure(new IllegalArgumentException(s"'$identifier' doesn't match matrix Room ID format"))
    }
  }
  def build(localPart: String, domainName: String): Try[RoomIdentifier] = {
    val built = RoomIdentifier(localPart, domainName)
    if (built.isValid)
      Success(built)
    else
      Failure(new IllegalAccessException(s"'$built' doesn't match matrix Room ID format"))
  }

  def generate(domainName: String): RoomIdentifier = RoomIdentifier(Codecs.genRoomId(), domainName)
}

object RoomAliasIdentifierUtils {
  private val roomIdPattern = """^#([\p{Alnum}\._=-]*):(.+)""".r
  def parse(identifier: String): Try[RoomAliasIdentifier] = {
    identifier match {
      case roomIdPattern(localName, domain) => Success(RoomAliasIdentifier(localName, domain))
      case _                                => Failure(new IllegalArgumentException(s"'$identifier' doesn't match matrix RoomAlias ID format"))
    }
  }
  def build(localPart: String, domainName: String): Try[RoomAliasIdentifier] = {
    val built = RoomAliasIdentifier(localPart, domainName)
    if (built.isValid)
      Success(built)
    else
      Failure(new IllegalAccessException(s"'$built' doesn't match matrix RoomAlias ID format"))
  }

  def generate(domainName: String): RoomAliasIdentifier = RoomAliasIdentifier(Codecs.genRoomId(), domainName)
}
