package org.mascarene.utils

import java.time.Instant

import io.circe.Json

object EventUtils {
  def addAge(originTs: Instant)(unsignedData: Json): Json = {
    unsignedData.deepMerge(Json.obj("age" -> Json.fromLong(Instant.now().toEpochMilli - originTs.toEpochMilli)))
  }

  def addTxnId(txnId: Option[String])(unsignedData: Json): Json = {
    txnId match {
      case None     => unsignedData
      case Some(tx) => unsignedData.deepMerge(Json.obj("transaction_id" -> Json.fromString(tx)))
    }
  }
}
