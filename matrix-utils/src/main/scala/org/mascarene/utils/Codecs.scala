/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.utils

import java.nio.ByteBuffer
import java.util.{Base64, UUID}

import org.apache.commons.codec.binary.Base32
import org.apache.commons.codec.digest.DigestUtils

object Codecs {
  def toBase64(uuid: UUID, unpadded: Boolean): String = {
    val (high, low) =
      (uuid.getMostSignificantBits, uuid.getLeastSignificantBits)
    val buffer = ByteBuffer.allocate(java.lang.Long.BYTES * 2)
    buffer.putLong(high)
    buffer.putLong(low)
    toBase64(buffer.array(), unpadded)
  }

  def toBase32(uuid: UUID, unpadded: Boolean): String = {
    val (high, low) =
      (uuid.getMostSignificantBits, uuid.getLeastSignificantBits)
    val buffer = ByteBuffer.allocate(java.lang.Long.BYTES * 2)
    buffer.putLong(high)
    buffer.putLong(low)
    toBase32(buffer.array(), unpadded)
  }

  def toBase64(bytes: Array[Byte], unpadded: Boolean): String =
    unpadded match {
      case false => Base64.getUrlEncoder.encodeToString(bytes)
      case true  => Base64.getUrlEncoder.encodeToString(bytes).split("=")(0)
    }

  def toBase32(bytes: Array[Byte], unpadded: Boolean): String = {
    val base = new Base32()
    unpadded match {
      case false => base.encodeAsString(bytes)
      case true  => base.encodeAsString(bytes).split("=")(0)
    }

  }

  def base64Encode(str: String, unpadded: Boolean = false): String = toBase64(str.getBytes, unpadded)
  def base64Decode(str: String): String                            = new String(Base64.getUrlDecoder.decode(str))

  def toHex(bytes: Array[Byte], sep: String = ""): String =
    bytes.map("%02x".format(_)).mkString(sep)

  private def fromHex(hex: String): Array[Byte] = {
    hex
      .replaceAll("[^0-9A-Fa-f]", "")
      .toSeq
      .sliding(2, 2)
      .map(_.unwrap)
      .map(Integer.parseInt(_, 16).toByte)
      .toArray
  }

  def randomUUID: String = toBase64(UUID.randomUUID(), false)

  def genSessionId(): String = toBase64(UUID.randomUUID(), true)

  def genDeviceId(): String = toBase64(UUID.randomUUID(), true)

  def genAccessToken(): String = toBase64(UUID.randomUUID(), true)

  def genUserId(): String = toBase32(UUID.randomUUID(), true).toLowerCase()

  def genRoomId(): String = toBase64(UUID.randomUUID(), true)

  def genId(): String = toBase64(UUID.randomUUID(), true)

  def sha256Hex(str: String): String    = DigestUtils.sha256Hex(str)
  def sha256(str: String): Array[Byte]  = DigestUtils.sha256(str)
  def sha256Base64(str: String): String = toBase64(DigestUtils.sha256(str), unpadded = true)

}
