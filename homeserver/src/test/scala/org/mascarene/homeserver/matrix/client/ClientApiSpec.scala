/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.actor.typed.scaladsl.adapter._
import com.google.common.util.concurrent.RateLimiter
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.internal.auth.AuthSessionCache
import org.mascarene.homeserver.matrix.server.client.{ClientApiRoutes, Version}
import org.scalatest._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class ClientApiSpec
    extends AnyWordSpecLike
    with Matchers
    with FailFastCirceSupport
    with ScalatestRouteTest
    with TestEnv
    with BeforeAndAfter
    with BeforeAndAfterAll {

  before {
    flyway.clean()
    flyway.migrate()
  }

  override def afterAll(): Unit = testKit.shutdownTestKit()

  implicit def typedSystem = testKit.system
  override def createActorSystem(): akka.actor.ActorSystem =
    testKit.system.toClassic

  private val rateLimiter = RateLimiter.create(clientCallRate)

  runtimeContext.actorRegistry
    .put("AuthSessionCache", testKit.spawn(AuthSessionCache.apply, "AuthSessionCache").unsafeUpcast)

  "Client API" should {
    "return some version information" in {
      val clientApi = new ClientApiRoutes(typedSystem, rateLimiter)
      Get("/_matrix/client/versions") ~> clientApi.routes ~> check {
        val version = responseAs[Version]
        version shouldBe a[Version]
        version.versions should not be empty
      }
    }
  }
}
