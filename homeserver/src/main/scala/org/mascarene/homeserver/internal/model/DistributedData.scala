package org.mascarene.homeserver.internal.model

import io.circe.Json
import io.circe.parser.parse

case class DistributedData(key: String, value: Json)
