/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import java.time.Instant
import java.util.UUID
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import io.circe._
import io.getquill.Ord
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{
  Event,
  EventAuthEdge,
  EventContent,
  EventParentEdge,
  EventTransaction,
  User
}
import org.mascarene.matrix.client.r0.model.events.UnsignedData

import scala.concurrent.Future
import scala.util.Try
import scala.concurrent.ExecutionContext.Implicits.global

class EventRepo(implicit val runtimeContext: RuntimeContext) extends JsonCodec with InstantCodec {
  import runtimeContext.dbContext._

  /* query schema initialization */
  private val events            = quote(querySchema[Event]("events"))
  private val eventAuthsEdges   = quote(querySchema[EventAuthEdge]("event_auth_edges"))
  private val eventParentEdges  = quote(querySchema[EventParentEdge]("event_parent_edges"))
  private val eventContents     = quote(querySchema[EventContent]("event_contents"))
  private val users             = quote(querySchema[User]("users"))
  private val eventTransactions = quote(querySchema[EventTransaction]("event_transactions"))

  private implicit val udDecoder: Decoder[UnsignedData] =
    decoder((index, row) => decode[UnsignedData](row.getObject(index).toString).fold(error => throw error, fd => fd))

  def getEventsByMxIds(mxIds: Set[String], processed: Boolean = true, rejected: Boolean = false): Try[Set[Event]] =
    Try {
      run {
        events
          .filter(e => liftQuery(mxIds).contains(e.mxEventId))
          .filter(_.processed == lift(processed))
          .filter(_.rejected == lift(rejected))
      }.toSet
    }

  def getEventsByIds(ids: Set[UUID]): Try[Set[Event]] =
    Try {
      run { events.filter(e => liftQuery(ids).contains(e.eventId)) }.toSet
    }

  def getEventById(eventId: UUID): Try[Option[Event]] =
    Try {
      run(events.filter(_.eventId == lift(eventId))).headOption
    }

  def getEventByIdWithCache(eventId: UUID): Try[Option[Event]] =
    Try {
      runtimeContext.eventsCache.getIfPresent(eventId).orElse {
        getEventById(eventId)
          .map {
            case Some(e) =>
              runtimeContext.eventsCache.put(eventId, e)
              Some(e)
            case _ => None
          }
          .getOrElse(None)
      }
    }

  def getEventAtStreamOrder(streamOrderPosition: Long): Try[Option[Event]] =
    Try {
      run(events.filter(_.streamOrder == lift(streamOrderPosition))).headOption
    }

  def insertEvent(
      roomId: UUID,
      senderId: UUID,
      eventType: String,
      stateKey: Option[String] = None,
      content: Option[Json] = None,
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      rejected: Boolean = false,
      resolved: Boolean = false,
      processed: Boolean = false,
      rejectionCause: Option[String] = None,
      mxEventId: String,
      unsignedData: Option[Json] = None,
      originServerTs: Instant,
      receivedTs: Option[Instant] = None,
      streamOrder: Long
  ): Try[Event] = {
    val newEvent = Event(
      UUID.randomUUID(),
      mxEventId,
      stateKey,
      originServerTs,
      receivedTs,
      senderId,
      roomId,
      eventType,
      unsignedData,
      rejected,
      resolved,
      processed,
      None,
      streamOrder,
      rejectionCause,
      Instant.now()
    )

    transaction[Try[Event]] {
      for {
        event <- Try {
          run(events.insert(lift(newEvent)))
          newEvent
        }
        eventContent <- insertEventContent(newEvent, content)
        parentEdges  <- insertParentEdgesAssoc(newEvent, parentsId)
        authEdges    <- insertAuthEdgesAssoc(newEvent, authsEventId)
      } yield event
    }
  }

  /**
    * Insert an event transaction
    * @param eventId eventId of the transacted event
    * @param transactionId transaction ID provided
    * @return instance of EventTransaction
    */
  def insertEventTransaction(eventId: UUID, transactionId: UUID): Try[EventTransaction] =
    Try {
      val newEventTransaction = EventTransaction(eventId, transactionId)
      run { eventTransactions.insert(lift(newEventTransaction)) }
      newEventTransaction
    }

  /**
    * Insert an event content
    * @param event event for which the content is created
    * @param content event content
    * @return instance of EventContent created
    */
  def insertEventContent(event: Event, content: Option[Json]): Try[EventContent] =
    Try {
      val newEventContent = EventContent(event.eventId, content)
      run { eventContents.insert(lift(newEventContent)) }
      newEventContent
    }

  /**
    * Create associations between an event and its parents.
    * @param event event to associate
    * @param parentsId Set of matrixId or UUID to associate (UUID may not exist if parent is not none)
    * @return number of inserted rows
    */
  def insertParentEdgesAssoc(event: Event, parentsId: Set[UUID]): Try[Set[EventParentEdge]] =
    Try {
      val edges = parentsId.map(parentId => EventParentEdge(event.eventId, parentId))
      run {
        quote {
          liftQuery(edges).foreach { edge => eventParentEdges.insert(edge) }
        }
      }
      edges
    }

  /**
    * Create associations between an event and its auth events.
    * @param event event to associate
    * @param authEventIds Map of matrixId or UUID to associate (UUID may not exist if auth event is not none)
    * @return number of inserted rows
    */
  def insertAuthEdgesAssoc(event: Event, authEventIds: Set[UUID]): Try[Set[EventAuthEdge]] =
    Try {
      val edges = authEventIds.map(authEventId => EventAuthEdge(event.eventId, authEventId))
      run {
        quote {
          liftQuery(edges).foreach { edge => eventAuthsEdges.insert(edge) }
        }
      }
      edges
    }

  /**
    * Update an event by setting if matrixId
    * @param eventId ID of the event to update
    * @param mxEventId matrixID to set
    * @return number of updated rows (should be 0 or 1)
    */
  def updateEventMxId(eventId: UUID, mxEventId: String): Try[Long] =
    Try {
      run(events.filter(_.eventId == lift(eventId)).update(_.mxEventId -> lift(mxEventId)))
    }.map { l =>
      runtimeContext.eventsCache.invalidate(eventId)
      l
    }

  def isEventRejected(eventId: UUID): Try[Option[Boolean]] =
    Try {
      run(events.filter(_.eventId == lift(eventId)).map(_.rejected)).headOption
    }

  def rejectEvent(eventId: UUID, cause: String): Try[Long] =
    Try {
      run {
        events
          .filter(_.eventId == lift(eventId))
          .update(_.rejected -> true, _.rejectionCause -> lift(Some(cause): Option[String]))
      }
    }.map { l =>
      runtimeContext.eventsCache.invalidate(eventId)
      l
    }

  def updateEventResolved(eventId: UUID, resolved: Boolean): Try[Long] =
    Try {
      run(events.filter(_.eventId == lift(eventId)).update(_.resolved -> lift(resolved)))
    }.map { l =>
      runtimeContext.eventsCache.invalidate(eventId)
      l
    }

  def updateEventProcessed(eventId: UUID, processed: Boolean): Try[Long] =
    Try {
      run(events.filter(_.eventId == lift(eventId)).update(_.processed -> lift(processed)))
    }.map { l =>
      runtimeContext.eventsCache.invalidate(eventId)
      l
    }

  def updateEventStateSetVersion(eventIds: Set[UUID], statesetVersion: Long): Try[Long] =
    Try {
      run {
        quote {
          liftQuery(eventIds).foreach { id =>
            events.filter(_.eventId == id).update(_.statesetVersion -> lift(Some(statesetVersion): Option[Long]))
          }
        }
      }.sum
    }.map { l =>
      runtimeContext.eventsCache.invalidateAll(eventIds)
      l
    }

  /**
    * get set of events which auth the given event
    * @param eventId
    * @return
    */
  def getAuthEvents(eventId: UUID): Try[Set[Event]] =
    Try {
      run {
        eventAuthsEdges.filter(_.eventId == lift(eventId)).join(events).on(_.authEventId == _.eventId).map {
          case (_, authEvent) => authEvent
        }
      }.toSet
    }

  def getAuthEventsWithCache(eventId: UUID): Try[Set[Event]] =
    Try {
      runtimeContext.eventAuthEdgesCache
        .getIfPresent(eventId)
        .orElse {
          getAuthEvents(eventId).map { authEvents =>
            Future { runtimeContext.eventsCache.putAll(authEvents.map(ev => ev.eventId -> ev).toMap) }
            runtimeContext.eventAuthEdgesCache.put(eventId, authEvents)
            Some(authEvents)
          }.get
        }
        .getOrElse(Set.empty)
    }

  /**
    * Get set of an event's parents
    * @param eventId
    * @return
    */
  def getParentEvents(eventId: UUID): Try[Set[Event]] =
    Try {
      run {
        eventParentEdges.filter(_.eventId == lift(eventId)).join(events).on(_.parentEventId == _.eventId).map {
          case (_, authEvent) => authEvent
        }
      }.toSet
    }

  def getParentEventsWithCache(eventId: UUID): Try[Set[Event]] =
    Try {
      runtimeContext.eventParentEdgesCache
        .getIfPresent(eventId)
        .orElse {
          getParentEvents(eventId).map { parentEvents =>
            Future { runtimeContext.eventsCache.putAll(parentEvents.map(ev => ev.eventId -> ev).toMap) }
            runtimeContext.eventParentEdgesCache.put(eventId, parentEvents)
            Some(parentEvents)
          }.get
        }
        .getOrElse(Set.empty)
    }

  def getEventContent(eventId: UUID): Try[Option[EventContent]] =
    Try {
      run {
        eventContents.filter(_.eventId == lift(eventId))
      }.headOption
    }

  def getEventsContent(eventsIds: List[UUID]): Try[Map[Event, EventContent]] =
    Try {
      run {
        events
          .filter(ev => liftQuery(eventsIds).contains(ev.eventId))
          .join(eventContents)
          .on(_.eventId == _.eventId)
          .map { case (ev, ec) => ev -> ec }
      }.toMap
    }

  def getEventsSenders(eventsIds: List[UUID]): Try[Map[Event, User]] =
    Try {
      run {
        events
          .filter(event => liftQuery(eventsIds).contains(event.eventId))
          .join(users)
          .on(_.senderId == _.userId)
          .map { case (event, user) => event -> user }
      }.toMap
    }

  def getEventsTransactions(eventsIds: List[UUID]): Try[Map[Event, EventTransaction]] =
    Try {
      run {
        events
          .filter(event => liftQuery(eventsIds).contains(event.eventId))
          .join(eventTransactions)
          .on(_.eventId == _.eventId)
          .map { case (event, eventTransation) => event -> eventTransation }
      }.toMap
    }

  def getRoomEvents(roomId: UUID): Try[Iterable[Event]] =
    Try {
      run(events.filter(_.roomId == lift(roomId)))
    }

  def getUnresolvedEvents(roomId: UUID): Try[Iterable[Event]] =
    Try {
      run(events.filter(_.roomId == lift(roomId)).filter(_.resolved == false).sortBy(e => e.streamOrder)(Ord.asc))
    }

  def getUnProcessedEvents(roomId: UUID): Try[Iterable[Event]] =
    Try {
      run(events.filter(_.roomId == lift(roomId)).filter(_.processed == false))
    }
}
