/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import cats.data.EitherT
import com.typesafe.scalalogging.LazyLogging
import cats.implicits._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, StateSet}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.jdk.DurationConverters._
import scala.concurrent.Future

class MRoomRedactionEventValidator(implicit runtimeContext: RuntimeContext) extends EventValidator with LazyLogging {

  private val defaultAwaitTimeout =
    runtimeContext.config.getDuration("mascarene.internal.default-await-timeout").toScala

  def validate(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)
    val f2 = validateSenderHasJoined(event, stateSet)
    val f3 = validateRequiredPowerLevel(event, stateSet)
    val f4 = validateStateKeyFormat(event)

    val f5 = EitherT[Future, EventAuthValidation, Event] {
      val currentPlContentFuture = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val senderFuture           = Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
      val senderPLFuture =
        senderFuture.flatMap(sender => Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet)))

      val future = for {
        sender           <- senderFuture
        senderPl         <- senderPLFuture
        currentPlContent <- currentPlContentFuture
      } yield (sender, senderPl, currentPlContent)
      future
        .map {
          case (Some(sender), Some(senderPl), Some(currentPl)) =>
            if (senderPl >= currentPl.redact) Right(event)
            else
              Left(InsufficientPowerLevels(event, sender.mxUserId, "redact", senderPl, currentPl.redact))

          case other => Left(InternalError(event, s"Couldn't check power level changes: unexpected $other"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't check power level changes: ${f.getMessage}")))
    }

    for {
      _ <- f1
      _ <- f2
      _ <- f3
      _ <- f4
      _ <- f5
    } yield event
  }
}
