/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.federation.client.r0.api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.HttpsConnectionContext
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model.headers.CacheDirectives.`max-age`
import akka.http.scaladsl.model.headers.{Host, `Cache-Control`}
import akka.http.scaladsl.model.{HttpHeader, Uri}
import akka.io.dns.DnsProtocol
import akka.io.dns.DnsProtocol.Srv
import akka.io.{Dns, IO}
import akka.util.Timeout
import com.google.common.net.{HostAndPort, InetAddresses}
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.federation.client.{KeySpec, SignedHttpTransport}
import org.mascarene.matrix.server.r0.model._

import java.time.Instant
import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

class FederationClient(
    val runtimeContext: RuntimeContext,
    val keySpec: KeySpec,
    authority: String,
    defaultResolveCacheTime: FiniteDuration,
    val origin: String,
    val connectionContext: Option[HttpsConnectionContext] = None
)(implicit
    val actorSystem: ActorSystem[Nothing],
    askTimeOut: Timeout
) extends LazyLogging
    with SignedHttpTransport {

  case class ResolvedServer(apiRoot: Uri, hostHeader: HttpHeader)

  private[this] var cachedResolvedServer: Option[ResolvedServer] = None
  private[this] var resolvedServerExpiration                     = Instant.now()

  private val scheme              = "https"
  protected def federationService = runtimeContext.newFederationService

  def resolveServerName: Future[ResolvedServer] = {
    if (Instant.now().isBefore(resolvedServerExpiration) && cachedResolvedServer.isDefined)
      Future.successful(cachedResolvedServer.get)
    else {
      val host = HostAndPort.fromString(authority)
      if (InetAddresses.isInetAddress(host.getHost)) {
        // the serverName is an IP literal
        if (host.hasPort)
          Future.successful(
            ResolvedServer(Uri(s"$scheme://${host.getHost}:${host.getPort}/"), Host(host.getHost, host.getPort))
          )
        else
          Future.successful(ResolvedServer(Uri(s"$scheme://${host.getHost}:8448/"), Host(host.getHost)))
      } else {
        // the serverName is not an IP literal
        if (host.hasPort) {
          Future.successful(
            ResolvedServer(Uri(s"$scheme://${host.getHost}:${host.getPort}/"), Host(host.getHost, host.getPort))
          )
        } else {
          logger.debug(s"$authority resolution requires well-known call for delegate")
          getWellKnown(Some(host.getHost))
            .flatMap {
              case (cacheControl, wkResponse) =>
                val delegated = HostAndPort.fromString(wkResponse.`m.server`)
                if (InetAddresses.isInetAddress(delegated.getHost)) {
                  // the serverName is an IP literal
                  if (delegated.hasPort)
                    Future.successful(
                      (
                        cacheControl,
                        ResolvedServer(
                          Uri(s"$scheme://${delegated.getHost}:${delegated.getPort}/"),
                          Host(delegated.getHost, delegated.getPort)
                        )
                      )
                    )
                  else
                    Future.successful(
                      (cacheControl, ResolvedServer(Uri(s"$scheme://${host.getHost}:8448/"), Host(delegated.getHost)))
                    )
                } else {
                  // the serverName is not an IP literal
                  if (delegated.hasPort) {
                    Future.successful(
                      (
                        cacheControl,
                        ResolvedServer(
                          Uri(s"$scheme://${delegated.getHost}:${delegated.getPort}/"),
                          Host(delegated.getHost, delegated.getPort)
                        )
                      )
                    )
                  } else {
                    // <delegated_hostname> is not an IP literal and no <delegated_port> is present
                    // an SRV record is looked up for _matrix._tcp.<delegated_hostname>.
                    getSrvRecord(delegated).map(serverName => (cacheControl, serverName))
                  }
                }
            }
            .map {
              case (cacheControl, resolvedName) =>
                logger.debug(
                  s"Added server resolution mapping : $authority -> $resolvedName for $cacheControl"
                )
                cachedResolvedServer = Some(resolvedName)
                resolvedServerExpiration = Instant.now().plusSeconds(cacheControl.toSeconds)
                resolvedName
            }
        }
      }
    }
  }

  /**
    * GET call to /.well-known/matrix/server
    *
    * @return Cache-Control duration and Well known response
    */
  def getWellKnown(targetServer: Option[String]): Future[(FiniteDuration, WellKnownResponse)] = {
    val wellKnownUri: Future[Uri] = targetServer match {
      case Some(serverName) => Future.successful(Uri(s"https://$serverName"))
      case None             => resolveServerName.map(_.apiRoot)
    }
    for {
      uri <- wellKnownUri
      wellKnownResponse <-
        doGet[WellKnownResponse](uri.withPath(Path("/.well-known/matrix/server")), Seq(Host(authority)))
      extractedCacheControl <- Future.successful(extractCacheValue(wellKnownResponse.headers))
    } yield (extractedCacheControl, wellKnownResponse.entity)
  }

  /**
    * GET call to /_matrix/federation/v1/version
    */
  def getFederationVersion: Future[ServerVersionResponse] =
    for {
      serverVersionUri <- resolveServerName.map(root =>
        root.copy(apiRoot = root.apiRoot.withPath(Path("/_matrix/federation/v1/version")))
      )
      serverVersionResponse <- doGet[ServerVersionResponse](serverVersionUri.apiRoot, Seq(serverVersionUri.hostHeader))
    } yield serverVersionResponse.entity

  /**
    * GET call to /_matrix/key/v2/server/{keyId} (keyId is deprecated)
    * @return
    */
  def getServerKeys: Future[ServerKeysResponse] =
    for {
      serverUri <-
        resolveServerName.map(root => root.copy(apiRoot = root.apiRoot.withPath(Path("/_matrix/key/v2/server/"))))
      serverVersionResponse <- doGet[ServerKeysResponse](serverUri.apiRoot, Seq(serverUri.hostHeader))
    } yield serverVersionResponse.entity

  /**
    * GET call to /_matrix/key/v2/query/{serverName}/{keyId}
    * @return
    */
  def getServerKeys(
      serverName: String,
      minimumValidUntil: Option[Instant] = None
  ): Future[ServerKeysResponse] = {
    val queryParam: Map[String, String] = minimumValidUntil match {
      case None          => Map.empty
      case Some(instant) => Map("minimum_valid_until_ts" -> instant.toEpochMilli.toString)
    }
    for {
      serverUri <- resolveServerName.map(root =>
        root.copy(apiRoot = root.apiRoot.withPath(Path(s"/_matrix/key/v2/server/query/$serverName")))
      )
      serverVersionResponse <- doGet[ServerKeysResponse](serverUri.apiRoot, Seq(serverUri.hostHeader), queryParam)
    } yield serverVersionResponse.entity
  }

  def getPublicRooms(
      limit: Option[Int] = None,
      since: Option[String] = None,
      includeAllNetworks: Option[Boolean],
      thirdPartyInstanceId: Option[String] = None
  ): Future[ListPublicRoomsResponse] = {

    val queries = mutable.Map[String, String]()
    limit.foreach(l => queries.addOne("limit" -> l.toString))
    since.foreach(s => queries.addOne("since" -> s))
    includeAllNetworks.foreach(i => queries.addOne("include_all_networks" -> i.toString))
    thirdPartyInstanceId.foreach(t => queries.addOne("third_party_instance_id" -> t))

    for {
      serverUri <- resolveServerName.map(root =>
        root.copy(apiRoot = root.apiRoot.withPath(Path(s"/_matrix/federation/v1/publicRooms")))
      )
      response <- doGet[ListPublicRoomsResponse](serverUri.apiRoot, Seq.empty, queries.toMap)
    } yield response.entity
  }

  def postPublicRooms(
      limit: Option[Int] = None,
      since: Option[String] = None,
      searchTerm: Option[String] = None,
      includeAllNetworks: Option[Boolean] = None,
      thirdPartyInstanceId: Option[String] = None
  ): Future[ListPublicRoomsResponse] = {

    val request =
      ListPublicRoomsRequest(
        limit,
        since,
        searchTerm.map(s => Filter(Some(s))),
        includeAllNetworks,
        thirdPartyInstanceId
      )
    for {
      serverUri <- resolveServerName.map(root =>
        root.copy(apiRoot = root.apiRoot.withPath(Path(s"/_matrix/federation/v1/publicRooms")))
      )
      response <- doPost[ListPublicRoomsRequest, ListPublicRoomsResponse](
        serverUri.apiRoot,
        request,
        Seq.empty,
        Map.empty,
        requireAuth = true
      )
    } yield response.entity
  }

  private def getSrvRecord(service: HostAndPort): Future[ResolvedServer] = {
    import akka.actor.typed.scaladsl.adapter._
    import akka.io.dns.SRVRecord
    import akka.pattern.ask

    implicit val classicSystem = actorSystem.toClassic

    (IO(Dns) ? DnsProtocol.Resolve(s"_matrix._tcp.${service.getHost}", Srv))
      .mapTo[DnsProtocol.Resolved]
      .map { resolved =>
        val srvRecord = resolved.records.head.asInstanceOf[SRVRecord]
        ResolvedServer(Uri(s"https://${srvRecord.target}:${srvRecord.port}/"), Host(srvRecord.target, srvRecord.port))
      }
      .fallbackTo {
        if (service.hasPort)
          Future.successful(
            ResolvedServer(
              Uri(s"https://${service.getHost}:${service.getPort}/"),
              Host(service.getHost, service.getPort)
            )
          )
        else
          Future.successful(ResolvedServer(Uri(s"https://${service.getHost}:8448/"), Host(service.getHost)))
      }
  }

  private def extractCacheValue(headers: Seq[HttpHeader]): FiniteDuration = {
    val headerValue = headers
      .collect {
        case h: `Cache-Control` => h
      }
      .flatMap(headers => headers.directives)
      .find(_.isInstanceOf[`max-age`])
    headerValue match {
      case Some(`max-age`(deltaSeconds)) => FiniteDuration(deltaSeconds, TimeUnit.SECONDS)
      case Some(_)                       => defaultResolveCacheTime
      case None                          => defaultResolveCacheTime
    }
  }
}
