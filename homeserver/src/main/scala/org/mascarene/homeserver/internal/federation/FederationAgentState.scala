package org.mascarene.homeserver.internal.federation

import org.mascarene.homeserver.internal.federation.client.KeySpec
import org.mascarene.homeserver.internal.model.federation.ServerKey

case class FederationAgentState(keySpec: KeySpec, serverKey: Option[ServerKey])
