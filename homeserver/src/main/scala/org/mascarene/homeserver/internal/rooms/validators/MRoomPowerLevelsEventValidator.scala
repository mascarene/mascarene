/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import cats.data.EitherT
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.internal.rooms.PowerLevelsUtils
import cats.implicits._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, StateSet}
import org.mascarene.matrix.client.r0.model.rooms.PowerLevelEventContent
import org.mascarene.utils.UserIdentifierUtils

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class InvalidPowerLevelContent(event: Event) extends EventAuthValidation with LazyLogging {
  override def rejectionCause: String =
    "InvalidPowerLevelContent: event content contains invalid power level definitions (see users keys)"
}

class MRoomPowerLevelsEventValidator(implicit runtimeContext: RuntimeContext) extends EventValidator with LazyLogging {

  private def validateContent(
      event: Event,
      powerLevelContent: PowerLevelEventContent
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future {
        if (powerLevelContent.users.keys.exists(eventType => UserIdentifierUtils.parse(eventType).isFailure)) {
          Left(InvalidPowerLevelContent(event))
        } else {
          Right(event)
        }
      }
    }

  private def validatePowerLevelChanges(
      event: Event,
      stateSet: StateSet,
      newPlContent: PowerLevelEventContent
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val currentPlContentFuture = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val senderFuture           = Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
      val senderPLFuture =
        senderFuture.flatMap(sender => Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet)))

      val future = for {
        sender           <- senderFuture
        senderPl         <- senderPLFuture
        currentPlContent <- currentPlContentFuture
      } yield (sender, senderPl, currentPlContent)

      future
        .map {
          case (Some(_), _, None) => Right(event) //No PL in room
          case (Some(sender), Some(senderPl), Some(currentPl)) =>
            val checks: Map[String, (Option[Int], Option[Int])] = Map(
              "users_default"  -> (currentPl.users_default, newPlContent.users_default),
              "events_default" -> (Some(currentPl.events_default), Some(newPlContent.events_default)),
              "state_default"  -> (Some(currentPl.state_default), Some(newPlContent.state_default)),
              "ban"            -> (Some(currentPl.ban), Some(newPlContent.ban)),
              "redact"         -> (Some(currentPl.redact), Some(newPlContent.redact)),
              "kick"           -> (Some(currentPl.kick), Some(newPlContent.kick)),
              "invite"         -> (Some(currentPl.invite), Some(newPlContent.invite))
            )
            val ret = checks.map {
              case (powerKey, (currentPl, newPl)) =>
                if (currentPl != newPl) {
                  val requiredPl = (currentPl ++ newPl).minOption
                  if (requiredPl.isDefined && requiredPl.get > senderPl)
                    Left(InsufficientPowerLevels(event, sender.mxUserId, powerKey, senderPl, requiredPl.get))
                  else
                    Right(event)
                } else
                  Right(event)
            }
            ret.find(_.isLeft).getOrElse(Right(event))
          case other => Left(InternalError(event, s"Couldn't check power level changes: unexpected $other"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't check power level changes: ${f.getMessage}")))
    }

  def validate(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)
    val f2 = validateSenderHasJoined(event, stateSet)
    val f3 = validateRequiredPowerLevel(event, stateSet)
    val f4 = validateStateKeyFormat(event)

    val eventContentFuture = for {
      eventContent <- Future.fromTry(eventRepo.getEventContent(event.eventId))
      content      <- Future { PowerLevelsUtils.getPlContent(eventContent.get) }
    } yield content

    val f5 = EitherT.right(eventContentFuture).flatMap { powerLevelContent =>
      validateContent(event, powerLevelContent)
        .flatMap(_ => validatePowerLevelChanges(event, stateSet, powerLevelContent))
    }

    for {
      _ <- f1
      _ <- f2
      _ <- f3
      _ <- f4
      _ <- f5
    } yield event
  }
}
