/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, Room, StateSet}
import org.mascarene.homeserver.internal.repository.AuthRepo

import scala.util.Try

class V1RoomImpl(room: Room)(implicit runtimeContext: RuntimeContext) extends RoomVersionImpl with LazyLogging {

  protected[this] val authRepo = new AuthRepo

  /**
    * Authorize event according to https://matrix.org/docs/spec/rooms/v1#id3
    * @param resolvedEvent
    * @return
    */
  override def resolve(resolvedEvent: Event): (Event, Try[StateSet]) = {
    logger.error("V1 algorithm is obsolete, not implemented")
    throw new NotImplementedError("V1 algorithm is obsolete, not implemented")
  }
}

object V1RoomImpl {
  def apply(room: Room)(implicit runtimeContext: RuntimeContext) =
    new V1RoomImpl(room)
}
