package org.mascarene.homeserver.internal.federation

import org.mascarene.matrix.server.r0.model.ListPublicRoomsResponse

import scala.util.Try

sealed trait FederationAgentResponse
case class PublicRoomResponse(publicRoomResponse: Try[ListPublicRoomsResponse]) extends FederationAgentResponse
