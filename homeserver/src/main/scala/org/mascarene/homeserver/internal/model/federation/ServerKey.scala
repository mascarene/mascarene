package org.mascarene.homeserver.internal.model.federation

import java.time.Instant
import java.util.UUID

case class ServerKey(
    serverKeyId: UUID,
    serverName: String,
    keyId: String,
    publicKey: Array[Byte],
    expiredAt: Option[Instant],
    validUntil: Option[Instant],
    nextManagerRefresh: Option[Instant],
    createdAt: Instant,
    updatedAt: Option[Instant]
)
