package org.mascarene.homeserver.internal.model

import java.time.Instant
import java.util.UUID

case class Device(
    deviceId: UUID,
    mxDeviceId: String,
    displayName: String,
    accountId: UUID,
    lastSeen: Option[Instant],
    createdAt: Instant,
    updatedAt: Option[Instant]
)
