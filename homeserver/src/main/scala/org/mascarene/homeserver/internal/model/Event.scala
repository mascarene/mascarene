/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.model

import io.circe.Json

import java.time.Instant
import java.util.UUID

case class Event(
    eventId: UUID,
    mxEventId: String,
    stateKey: Option[String],
    originServerTs: Instant,
    receivedTs: Option[Instant],
    senderId: UUID,
    roomId: UUID,
    eventType: String,
    unsigned: Option[Json],
    rejected: Boolean,
    resolved: Boolean,
    processed: Boolean,
    statesetVersion: Option[Long],
    streamOrder: Long,
    rejectionCause: Option[String],
    createdAt: Instant
) {
  def isStateEvent: Boolean = stateKey.isDefined
}
