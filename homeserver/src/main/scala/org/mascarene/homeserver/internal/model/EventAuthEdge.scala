package org.mascarene.homeserver.internal.model

import java.util.UUID

case class EventAuthEdge(eventId: UUID, authEventId: UUID)
