package org.mascarene.homeserver.internal.model

object EventTypes {
  val M_ROOM_CREATE             = "m.room.create"
  val M_ROOM_MEMBER             = "m.room.member"
  val M_ROOM_POWER_LEVELS       = "m.room.power_levels"
  val M_ROOM_JOIN_RULES         = "m.room.join_rules"
  val M_ROOM_HISTORY_VISIBILITY = "m.room.history_visibility"
  val M_ROOM_GUEST_ACCESS       = "m.room.guest_access"
  val M_ROOM_NAME               = "m.room.name"
  val M_ROOM_TOPIC              = "m.room.topic"
  val M_ROOM_ALIASES            = "m.room.aliases"
  val M_ROOM_THIRD_PARTY_INVITE = "m.room.third_party_invite"
  val M_ROOM_REDACTION          = "m.room.redaction"
  val M_ROOM_CANONICAL_ALIAS    = "m.room.canonical_alias"
  val M_ROOM_AVATAR             = "m.room.avatar"
}
