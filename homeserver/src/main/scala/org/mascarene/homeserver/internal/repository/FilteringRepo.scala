/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import java.sql.PreparedStatement
import java.time.Instant
import java.util.UUID
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Account, Filter}

import scala.util.Try

class FilteringRepo(implicit val runtimeContext: RuntimeContext) extends InstantCodec {
  import runtimeContext.dbContext._

  private val filters = quote(querySchema[Filter]("filters"))

  private implicit val fdDecoder: Decoder[FilterDefinition] =
    decoder((index, row) =>
      decode[FilterDefinition](row.getObject(index).toString).fold(error => throw error, fd => fd)
    )

  private implicit val fdEncoder: Encoder[FilterDefinition] = encoder[FilterDefinition](
    java.sql.Types.OTHER,
    (index: Int, value: FilterDefinition, row: PreparedStatement) =>
      row.setObject(index, value.asJson.noSpaces, java.sql.Types.OTHER)
  )

  def createFilter(accountId: UUID, filterDefinition: FilterDefinition): Try[Filter] =
    Try {
      val newRow = Filter(UUID.randomUUID(), accountId, Some(filterDefinition), Instant.now(), None)
      run {
        filters.insert(lift(newRow))
      }
      newRow
    }

  def getFilter(filterId: UUID): Try[Option[Filter]] =
    Try {
      run { filters.filter(_.filterId == lift(filterId)) }.headOption
    }
}
