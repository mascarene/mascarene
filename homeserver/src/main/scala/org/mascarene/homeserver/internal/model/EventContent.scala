package org.mascarene.homeserver.internal.model

import io.circe.Json

import java.util.UUID

case class EventContent(eventId: UUID, content: Option[Json] = None)
