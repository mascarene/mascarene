package org.mascarene.homeserver.internal.rooms

import akka.actor.typed.ActorRef
import org.mascarene.homeserver.internal.model.{Event, Room, StateSet}

import scala.util.Try

sealed trait RoomCommand
case class InitialState(room: Room)                                              extends RoomCommand
case class GetRoomState(replyTo: ActorRef[RoomStateResponse])                    extends RoomCommand
case class SetVisibility(visibility: String)                                     extends RoomCommand
case class UpdateState(event: Event, stateSet: Try[StateSet])                    extends RoomCommand
case class ProcessEvent(event: Event, replyTo: Option[ActorRef[EventProcessed]]) extends RoomCommand
