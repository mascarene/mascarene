package org.mascarene.homeserver.internal.federation

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.federation.FederationAgent.TypeKey

import scala.concurrent.ExecutionContextExecutor

object FederationCluster {
  def apply(implicit runtimeContext: RuntimeContext): Behavior[FederationClusterCommand] =
    Behaviors.setup { ctx => new FederationCluster(ctx) }
}

class FederationCluster(
    context: ActorContext[FederationClusterCommand]
)(implicit val runtimeContext: RuntimeContext)
    extends AbstractBehavior[FederationClusterCommand](context)
    with ImplicitAskTimeOut {
  implicit val ec: ExecutionContextExecutor = context.executionContext
  private val sharding                      = ClusterSharding(context.system)

  sharding.init(Entity(TypeKey) { entityContext =>
    FederationAgent(entityContext.entityId, runtimeContext)
  })

  override def onMessage(msg: FederationClusterCommand): Behavior[FederationClusterCommand] = {
    msg match {
      case GetFederationAgent(authority, replyTo) =>
        replyTo ! GotFederationAgent(sharding.entityRefFor(FederationAgent.TypeKey, authority))
        this
    }
  }
}
