package org.mascarene.homeserver.internal.federation.client

import akka.http.scaladsl.model.{HttpHeader, Uri}
import io.circe.{Decoder, Encoder}
import org.mascarene.sdk.matrix.core.HttpApiResponse

import scala.concurrent.Future

trait Transport {
  protected def doDelete[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit decoder: Decoder[R]): Future[HttpApiResponse[R]]

  protected def doGet[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit decoder: Decoder[R]): Future[HttpApiResponse[R]]

  protected def doPost[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit encoder: Encoder[Q], decoder: Decoder[R]): Future[HttpApiResponse[R]]

  protected def doPut[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit encoder: Encoder[Q], decoder: Decoder[R]): Future[HttpApiResponse[R]]
}
