/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import io.getquill.Ord
import org.mascarene.homeserver.RuntimeContext

import java.time.Instant
import java.util.UUID
import org.mascarene.homeserver.internal.model.{
  DistributedData,
  Event,
  Room,
  RoomAlias,
  RoomAliasServer,
  RoomIndex,
  RoomMembership,
  User
}

import scala.util.{Failure, Try}

class RoomRepo(implicit val runtimeContext: RuntimeContext)
    extends JsonCodec
    with InstantCodec
    with TextSearchOperator {
  import runtimeContext.dbContext._

  private val rooms              = quote(querySchema[Room]("rooms"))
  private val roomMemberships    = quote(querySchema[RoomMembership]("room_memberships"))
  private val events             = quote(querySchema[Event]("events"))
  private val roomAliases        = quote(querySchema[RoomAlias]("room_aliases"))
  private val roomAliasesServers = quote(querySchema[RoomAliasServer]("room_aliases_servers"))
  private val users              = quote(querySchema[User]("users"))
  private val roomIndices        = quote(querySchema[RoomIndex]("room_indices"))

  def getRoomByMxId(mxRoomId: String): Try[Option[Room]] =
    Try {
      run { rooms.filter(_.mxRoomId == lift(mxRoomId)) }.headOption
    }

  def getRoomById(id: UUID): Try[Option[Room]] =
    Try {
      run { rooms.filter(_.roomId == lift(id)) }.headOption
    }

  def getRoomByAlias(aliasName: String): Try[Option[Room]] =
    Try {
      run { roomAliases.filter(_.alias == lift(aliasName)).join(rooms).on(_.roomId == _.roomId).map(_._2) }.headOption
    }

  def createRoom(mxRoomId: String, visibility: String, version: String): Try[Room] =
    Try {
      val newRow = Room(UUID.randomUUID(), mxRoomId, visibility, version, -1, Instant.now(), None)
      run { rooms.insert(lift(newRow)) }
      newRow
    }

  def updateLastStateSetVersion(roomId: UUID, lastVersion: Long): Try[Room] = {
    val now = Instant.now()
    getRoomById(roomId).flatMap {
      case Some(room) =>
        Try {
          run(
            rooms
              .filter(_.roomId == lift(roomId))
              .update(_.lastStatesetVersion -> lift(lastVersion), _.updatedAt -> lift(Some(now): Option[Instant]))
          )
        }.map(_ => room.copy(lastStatesetVersion = lastVersion, updatedAt = Some(now)))
      case None => Failure(new IllegalArgumentException(s"No room found with id $roomId"))
    }
  }

  def createOrUpdateMembership(roomId: UUID, userId: UUID, eventId: UUID, membership: String): Try[RoomMembership] =
    Try {
      val now           = Instant.now()
      val newMembership = RoomMembership(roomId, userId, eventId, membership, now, None)
      val updatedAt = run {
        roomMemberships
          .insert(lift(newMembership))
          .onConflictUpdate(_.roomId, _.userId)(
            (t, e) => t.membership -> e.membership,
            (t, e) => t.eventId -> e.eventId,
            (t, e) => t.updatedAt -> Some(e.createdAt)
          )
          .returning(r => (r.eventId, r.updatedAt))
      }
      newMembership.copy(eventId = updatedAt._1, updatedAt = updatedAt._2)
    }

  def getRoomMemberships(userId: UUID): Try[Iterable[(RoomMembership, Room, Event)]] =
    Try {
      run {
        roomMemberships
          .filter(_.userId == lift(userId))
          .join(rooms)
          .on(_.roomId == _.roomId)
          .join(events)
          .on { case ((rm, _), e) => rm.eventId == e.eventId }
          .map { case ((rm, m), e) => (rm, m, e) }
      }
    }

  def getRoomMembership(roomId: UUID, userId: UUID): Try[Option[RoomMembership]] =
    Try {
      run {
        roomMemberships
          .filter(_.userId == lift(userId))
          .filter(_.roomId == lift(roomId))
      }.headOption
    }

  def getRoomMembershipWithMemberEvent(roomId: UUID, userId: UUID): Try[Option[(RoomMembership, Event)]] =
    Try {
      run {
        roomMemberships
          .filter(_.userId == lift(userId))
          .filter(_.roomId == lift(roomId))
          .join(events)
          .on(_.eventId == _.eventId)
      }.headOption
    }

  def forceMembership(roomId: UUID, userId: UUID, membership: String): Try[Long] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        roomMemberships
          .filter(_.userId == lift(userId))
          .filter(_.roomId == lift(roomId))
          .update(_.membership -> lift(membership), _.updatedAt -> lift(now))
      }
    }

  def getRoomMembers(roomId: UUID, membership: String): Try[Iterable[(User, Event)]] =
    Try {
      run {
        roomMemberships
          .filter(_.roomId == lift(roomId))
          .filter(_.membership == lift(membership))
          .join(users)
          .on(_.userId == _.userId)
          .join(events)
          .on { case ((rm, _), e) => rm.eventId == e.eventId }
          .map { case ((_, user), event) => (user, event) }
      }
    }

  /**
    * Get a room last event, ie: the event with the max streamOrder value among room events
    * @param roomId
    * @return
    */
  def getRoomLastEvent(roomId: UUID, processed: Boolean = true, rejected: Boolean = false): Try[Option[Event]] =
    Try {
      val maxStreamOrder: Long = run(
        events
          .filter(_.roomId == lift(roomId))
          .filter(_.processed == lift(processed))
          .filter(_.rejected == lift(rejected))
          .map(_.streamOrder)
          .max
      ).getOrElse(0L)
      run {
        //See https://github.com/getquill/quill/issues/1857 for a fix
        //val maxStreamOrder = events.filter(_.roomId == lift(roomId)).map(_.streamOrder).max.getOrElse(0L)
        events.filter(_.streamOrder == lift(maxStreamOrder))
      }.headOption
    }

  /**
    * Get the last room event before the given stream position
    * @param roomId
    * @return
    */
  def getRoomLastEventBeforeStreamPosition(roomId: UUID, streamOrderPosition: Long): Try[Option[Event]] =
    Try {
      val maxStreamOrder = run(
        events
          .filter(_.roomId == lift(roomId))
          .filter(_.streamOrder <= lift(streamOrderPosition))
          .map(_.streamOrder)
          .max
      ).getOrElse(0L)
      run {
        events.filter(_.streamOrder == lift(maxStreamOrder))
      }.headOption
    }

  /**
    * Query all room events having streamOrder > minStreamOrder and streamOrder <= maxStreamOrder
    * @param roomId
    * @param minStreamOder
    * @param maxStreamOrder
    * @return
    */
  def getRoomEvents(
      roomId: UUID,
      minStreamOder: Long = 0L,
      maxStreamOrder: Long = Long.MaxValue,
      processed: Boolean = true,
      rejected: Boolean = false
  ): Try[List[Event]] =
    Try {
      run {
        events
          .filter(_.roomId == lift(roomId))
          .filter(_.streamOrder > lift(minStreamOder))
          .filter(_.streamOrder <= lift(maxStreamOrder))
          .filter(_.processed == lift(processed))
          .filter(!_.rejected == lift(rejected))
          .sortBy(_.streamOrder)(Ord.asc)
      }
    }

  def getRoomAlias(alias: String): Try[Option[(RoomAlias, List[RoomAliasServer])]] =
    Try {
      run {
        roomAliases.filter(_.alias == lift(alias)).join(roomAliasesServers).on(_.aliasId == _.aliasId)
      }.groupBy(_._1).map(res => (res._1, res._2.map(_._2))).headOption
    }

  def getKnownAliases(roomId: UUID): Try[List[RoomAlias]] =
    Try {
      run {
        roomAliases.filter(_.roomId == lift(roomId)).join(roomAliasesServers).on(_.aliasId == _.aliasId)
      }.map(_._1)
    }

  def getKnownAliases(roomId: UUID, serverName: String): Try[List[RoomAlias]] =
    Try {
      run {
        roomAliases
          .filter(_.roomId == lift(roomId))
          .join(roomAliasesServers)
          .on(_.aliasId == _.aliasId)
          .filter(_._2.serverDomain == lift(serverName))
      }.map(_._1)
    }

  def createRoomAlias(
      roomId: UUID,
      creatorId: UUID,
      alias: String,
      serverDomain: String
  ): Try[(RoomAlias, RoomAliasServer)] = {
    getRoomAlias(alias).map {
      case Some((alias, _)) =>
        val newAliasServer = RoomAliasServer(alias.aliasId, serverDomain, Instant.now())
        run(roomAliasesServers.insert(lift(newAliasServer)).onConflictIgnore)
        (alias, newAliasServer)
      case None =>
        val now             = Instant.now()
        val roomAlias       = RoomAlias(UUID.randomUUID(), roomId, creatorId, alias, now)
        val roomAliasServer = RoomAliasServer(roomAlias.aliasId, serverDomain, now)
        transaction {
          run(roomAliases.insert(lift(roomAlias)))
          run(roomAliasesServers.insert(lift(roomAliasServer)))
        }
        (roomAlias, roomAliasServer)
    }
  }

  def updateRoomVisibility(roomId: UUID, visibility: String): Try[Instant] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        rooms.filter(_.roomId == lift(roomId)).update(_.visibility -> lift(visibility), _.updatedAt -> lift(now))
      }
      now.get
    }

  /**
    * Create or update a room index term
    * @param roomId Id of the room to create/update index for
    * @param eventType  term source event_type
    * @param indexTerm  indexed term
    * @return RoomIndex created or inserted
    */
  def createOrUpdateRoomIndex(roomId: UUID, eventType: String, indexTerm: String): Try[RoomIndex] =
    Try {
      val now       = Instant.now()
      val roomIndex = RoomIndex(roomId, eventType, indexTerm, now, None)
      val updatedAt = run {
        roomIndices
          .insert(lift(roomIndex))
          .onConflictUpdate(_.roomId, _.eventType)(
            (t, e) => t.indexTerm -> e.indexTerm,
            (t, e) => t.updatedAt -> Some(e.createdAt)
          )
          .returning(r => (r.createdAt, r.updatedAt))
      }
      roomIndex.copy(createdAt = updatedAt._1, updatedAt = updatedAt._2)
    }

  private def searchRoomIdsByIndexTerm(searchTerm: Option[String]) =
    searchTerm match {
      case Some(term) =>
        quote {
          roomIndices
            .filter(r => toTsVector(r.indexTerm) @@ toTsQuery(lift(term)))
        }
      case None =>
        quote {
          roomIndices
        }
    }

  /**
    * Search for room for which index terms match searchTerm
    * room index terms is composed of m.room.name, m.room.canonical_name, ... content
    * @param searchTerm
    * @return
    */
  def searchPublicRoomIndex[R](
      searchTerm: Option[String]
  ): Try[Seq[Room]] =
    Try {
      run {
        searchRoomIdsByIndexTerm(searchTerm)
          .join(rooms)
          .on(_.roomId == _.roomId)
          .map(_._2)
          .filter(_.visibility == "public")
      }.distinctBy(_.roomId).sortBy(_.roomId)
    }

  def searchPublicRooms: Try[List[Room]] =
    Try {
      run {
        rooms.filter(_.visibility == "public")
      }
    }
}
