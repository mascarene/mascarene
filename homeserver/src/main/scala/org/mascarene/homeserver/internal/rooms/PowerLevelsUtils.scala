/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import org.mascarene.matrix.client.r0.model.rooms.PowerLevelEventContent
import io.circe._
import io.circe.generic.auto._
import org.mascarene.homeserver.internal.model.EventContent

object PowerLevelsUtils {
  def findUserPowerLevel(userMxid: String, powerLevelContent: PowerLevelEventContent): Int = {
    if (powerLevelContent.users.contains(userMxid))
      powerLevelContent.users(userMxid)
    else {
      powerLevelContent.users_default.getOrElse(0)
    }
  }

  def requiredPowerLevelForEventType(eventType: String, powerLevelEventContent: PowerLevelEventContent): Int = {
    powerLevelEventContent.events.getOrElse(eventType, powerLevelEventContent.events_default)
  }

  def getPlContent(eventContent: EventContent): PowerLevelEventContent =
    eventContent.content.get.as[PowerLevelEventContent].getOrElse(defaultPowerLevelContent)

  val defaultPowerLevelContent: PowerLevelEventContent = PowerLevelEventContent(
    ban = 50,
    events = Map.empty,
    events_default = 0,
    invite = 50,
    kick = 50,
    redact = 50,
    state_default = 50,
    users = Map.empty,
    users_default = Some(0),
    notifications = Map.empty
  )
}
