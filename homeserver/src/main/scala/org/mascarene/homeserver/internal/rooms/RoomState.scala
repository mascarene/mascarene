package org.mascarene.homeserver.internal.rooms

import org.mascarene.homeserver.internal.model.{Event, Room, StateSet}

case class RoomState(
    room: Room,
    lastResolvedEvent: Option[Event] = None,
    lastReceivedEvent: Option[Event] = None,
    resolvedState: StateSet = StateSet()
)
