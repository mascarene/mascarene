/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import org.mascarene.homeserver.internal.model.Event

trait EventAuthValidation {
  def rejectionCause: String
}
case class RoomDomainMismatchSenderDomain(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = s"RoomDomainMismatchSenderDomain: Room Id domain doesn't match sender Id domain"
}
case class DuplicateEntriesInAuthEvents(event: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    s"DuplicateEntriesInAuthEvents: Event authchain contains duplicates state entries"
}
case class InvalidEventTypeInAuthChain(event: Event, invalidType: String) extends EventAuthValidation {
  override def rejectionCause: String =
    s"InvalidEventTypeInAuthChain: Event authchain contain invalid event type(s) $invalidType"
}
case class SenderDomainMismatchStateKey(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = s"SenderDomainMismatchStateKey: sender domain doesn't match state key"
}
case class InvalidContent(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = "InvalidContent: Event content not provided or couldn't be parsed"
}
case class ParentEventMustBeUniqueAndCreate(event: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    "ParentEventMustBeUniqueAndCreate: parent event is not unique or not a m.room.create event"
}
case class SenderMismatchStatekey(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = "SenderMismatchStatekey: sender doesn't match state key"
}
case class SenderIsBanned(event: Event, memberEvent: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    s"SenderIsBanned: sender has been banned according to event ${memberEvent.mxEventId}"
}
case class Unauthorized(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = s"Unauthorized: unauthorized events according to default rules"
}
case class InternalError(event: Event, message: String) extends EventAuthValidation {
  override def rejectionCause: String = s"InternalError: $message"
}
