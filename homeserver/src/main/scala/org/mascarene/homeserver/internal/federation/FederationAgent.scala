package org.mascarene.homeserver.internal.federation

import akka.actor.typed.{ActorSystem, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.federation.client.r0.api.FederationClient
import org.mascarene.homeserver.services.FederationService

import scala.jdk.DurationConverters._
import scala.util.{Failure, Success}

object FederationAgent {
  val TypeKey: EntityTypeKey[FederationAgentCommand] = EntityTypeKey[FederationAgentCommand]("FederationAgent")

  def apply(
      entityId: String,
      runtimeContext: RuntimeContext
  ): Behavior[FederationAgentCommand] =
    Behaviors.withStash(100) { buffer =>
      Behaviors.setup { ctx => new FederationAgent(entityId, ctx, buffer, runtimeContext).init() }
    }
}
class FederationAgent(
    authority: String,
    context: ActorContext[FederationAgentCommand],
    buffer: StashBuffer[FederationAgentCommand],
    implicit val runtimeContext: RuntimeContext
) extends LazyLogging
    with ImplicitAskTimeOut {
  implicit val system: ActorSystem[Nothing] = context.system
  implicit val ec                           = system.executionContext

  private val authorityNameCacheTime =
    runtimeContext.config.getDuration("mascarene.server.federation.server-resolve-cache-time")
  private val matrixServerName = runtimeContext.config.getString("mascarene.server.domain-name")

  private val keyManager = runtimeContext.actorRegistry("KeyManager").narrow[KeyManagerCommand]

  private val federationService: FederationService = runtimeContext.newFederationService

  private def init(): Behavior[FederationAgentCommand] = {
    context.ask(keyManager, ref => GetKeySpec(ref)) {
      case Success(GotKeySpec(keySpec)) =>
        val federationClient =
          new FederationClient(
            runtimeContext,
            keySpec,
            authority,
            authorityNameCacheTime.toScala,
            matrixServerName
          )
        InitialState(federationClient, FederationAgentState(keySpec, None))
      case Failure(f) => InitError(f)
    }
    Behaviors.receiveMessage {
      case InitialState(client, state) =>
        // now we are ready to handle stashed messages if any
        context.log.debug(s"federation agent for authority $authority init completed")
        buffer.unstashAll(active(client, state))
      case InitError(cause) => throw cause
      case other            =>
        // stash all other messages for later processing
        buffer.stash(other)
        Behaviors.same
    }
  }

  def active(federationClient: FederationClient, agentState: FederationAgentState): Behavior[FederationAgentCommand] = {
    Behaviors.receiveMessagePartial {
      case r: GetPublicRooms =>
        federationClient
          .getPublicRooms(r.limit, r.since, r.includeAllNetworks, r.thirdPartyInstanceId)
          .onComplete(response => r.replyTo ! PublicRoomResponse(response))
        Behaviors.same
      case r: PostPublicRooms =>
        federationClient
          .postPublicRooms(r.limit, r.since, r.searchTerm, r.includeAllNetworks, r.thirdPartyInstanceId)
          .onComplete(response => r.replyTo ! PublicRoomResponse(response))
        Behaviors.same

    }
  }

  /*
    federationService
      .getLocalKey(agentState.keySpec.keyId)
      .map {
        case Some(serverKey) => context.self ! InitialState(agentState.copy(serverKey = Some(serverKey)))
        case _ =>
          throw new IllegalArgumentException(
            s"Invalid agent state keyId '${agentState.keySpec.keyId}: no key found with this Id"
          )
      }
  }
   */
}
