/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.sdk.matrix.core.ApiFailure
import cats.data.EitherT
import cats.implicits._

import scala.concurrent.Future
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, EventTypes, StateSet}
import org.mascarene.matrix.client.r0.model.rooms.ThirdPartyInviteContent

import scala.util.{Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

case class NoThirdPartyInviteEvent(event: Event) extends EventAuthValidation with LazyLogging {
  override def rejectionCause: String =
    "NoThirdPartyInviteEvent: no m.room.third_party_invite event found in event state set"
}

case class SignatureNotFound(event: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    "SignatureNotFound: No signature matching signatures in third party invite event"
}

case class ThirdPartyInviteSenderMismatchInviteEventSender(event: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    "ThirdPartyInviteSenderMismatchInviteEventSender: no m.room.third_party_invite sender doesn't match invite event sender"
}

case class BadSenderMembership(event: Event, memberEvent: Event, expected: String) extends EventAuthValidation {
  override def rejectionCause: String =
    s"BadSenderMembership: sender doesn't have the expected '$expected' membership according to event ${memberEvent.mxEventId}"
}

case class BadTargetMembership(event: Event, memberEvent: Event, expected: String) extends EventAuthValidation {
  override def rejectionCause: String =
    s"BadTargetMembership: membership target user has an unexpected '$expected' membership according to event ${memberEvent.mxEventId}"
}

case class UnknownMembership(event: Event, membership: String) extends EventAuthValidation {
  override def rejectionCause: String = s"UnknownMembership: Unknown membership '$membership'"
}

case class InsufficientPowerLevels(event: Event, senderMxId: String, power: String, userPl: Int, requiredPl: Int)
    extends EventAuthValidation {
  override def rejectionCause: String =
    s"InsufficientPowerLevels: sender $senderMxId has insufficient '$power' power level: $userPl < $requiredPl"
}

class MRoomMemberEventValidator(implicit runtimeContext: RuntimeContext) extends EventValidator with LazyLogging {

  /**
    * Returns a user ban event if the stateset contains one for this user
    * @param userMxId
    * @param stateSet
    * @return
    */
  private def hasUserBannedEvent(userMxId: String, stateSet: StateSet): Try[Option[Event]] = {
    currentMembership(userMxId, stateSet).map {
      case Some((stateMemberEvent, memberContent)) =>
        if (memberContent.membership == "ban")
          Some(stateMemberEvent)
        else
          None
      case _ => None
    }
  }

  /**
    * Returns the current membership of an event
    * @param userMxId user to find membership
    * @param stateSet room state at event
    * @return a tuple containing the member event and its decoded content
    */
  private def currentMembership(userMxId: String, stateSet: StateSet): Try[Option[(Event, MemberEventContent)]] = {
    stateSet.getEvent(EventTypes.M_ROOM_MEMBER, userMxId) match {
      case None => Success(None)
      case Some(stateMemberEvent) =>
        val tryContent = for {
          eventContent <- eventRepo.getEventContent(stateMemberEvent.eventId)
          content <-
            eventContent.get.content.get
              .as[MemberEventContent]
              .toTry
        } yield content
        tryContent
          .map { memberContent => Some((stateMemberEvent, memberContent)) }
          .recoverWith {
            case f =>
              throw new ApiFailure(
                "ORG.MASCARENE.EVENT.BAD_CONTENT",
                s"membership event ${stateMemberEvent.mxEventId} content couldn't be decoded to MemberEventContent",
                None,
                f
              )
          }
    }
  }

  private def validateInviteMemberShip(
      event: Event,
      stateSet: StateSet,
      content: MemberEventContent
  ): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateInviteMemberShipContent(event, stateSet, content)
    val f2 = validateInviteMemberShipUsers(event, stateSet)
    val f3 = validateInviteMemberShipPL(event, stateSet)
    for {
      _ <- f1
      _ <- f2
      _ <- f3
    } yield event
  }

  /**
    * If the sender's power level is greater than or equal to the invite level, allow.
    * Otherwise, reject.
    *
    * @param stateSet
    * @param event
    * @return
    */
  private def validateInviteMemberShipPL(
      event: Event,
      stateSet: StateSet
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val f1 = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val futurePl = for {
        sender <- Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
        userPl <- Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet))
        pl     <- f1
      } yield (sender, userPl, pl)
      futurePl
        .map {
          case (Some(sender), Some(userPl), Some(statePl)) =>
            if (userPl >= statePl.invite)
              Right(event)
            else
              Left(InsufficientPowerLevels(event, sender.mxUserId, "invite", userPl, statePl.invite))
          case _ => Left(InternalError(event, "Couldn't get/decode sender or target power level events"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't validate invite membership event: ${f.getMessage}")))
    }

  private def validateInviteMemberShipUsers(
      event: Event,
      stateSet: StateSet
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val senderMembershipFuture = Future.fromTry(
        authRepo
          .getUserByIdWithCache(event.senderId)
          .flatMap(sender => currentMembership(sender.get.mxUserId, stateSet))
      )
      val targetUserMembershipFuture = Future.fromTry(currentMembership(event.stateKey.get, stateSet))
      val memberShipFuture = for {
        senderMembership     <- senderMembershipFuture
        targetUserMembership <- targetUserMembershipFuture
      } yield (senderMembership, targetUserMembership)
      memberShipFuture.map {
        case (Some((senderMemberShipEvent, senderMemberShip)), Some((targetMemberShipEvent, targetMemberShip))) =>
          if (senderMemberShip.membership != "join")
            Left(BadSenderMembership(event, senderMemberShipEvent, "join"))
          else {
            if (targetMemberShip.membership == "join" || targetMemberShip.membership == "ban")
              Left(BadTargetMembership(event, targetMemberShipEvent, targetMemberShip.membership))
            else
              Right(event)
          }
        case (Some(_), None) => Right(event)
        case _               => Left(InternalError(event, "Couldn't get/decode sender or target membership events"))
      }
    }

  private def validateInviteMemberShipContent(
      event: Event,
      stateSet: StateSet,
      content: MemberEventContent
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      if (content.third_party_invite.isEmpty) {
        Future.successful(Right(event))
      } else {
        // If content has third_party_invite key
        val bannedFuture = for {
          sender   <- Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
          banEvent <- Future.fromTry(hasUserBannedEvent(sender.get.mxUserId, stateSet))

        } yield (sender, banEvent)
        bannedFuture.map {
          case (None, None) =>
            Left(InternalError(event, s"Couldn't not get event sender or banned event (if it exists"))
          case (_, Some(banEvent)) => Left(SenderIsBanned(event, banEvent)) //If target user is banned, reject.
          case (Some(sender), None) =>
            if (content.third_party_invite.get.signed.mxid != event.stateKey.get)
              Left(ThirdPartyInviteSignatureMxIdMismatchStateKey(event)) //If mxid does not match state_key, reject.
            else {
              stateSet
                .getEvent(EventTypes.M_ROOM_THIRD_PARTY_INVITE, sender.mxUserId)
                .map { thirdPartyInviteEvent =>
                  if (thirdPartyInviteEvent.senderId != event.senderId)
                    Left(
                      ThirdPartyInviteSenderMismatchInviteEventSender(event)
                    ) //If sender does not match sender of the m.room.third_party_invite, reject.
                  else {
                    // If any signature in signed matches any public key in the m.room.third_party_invite event, allow.
                    // The public keys are in content of m.room.third_party_invite as:
                    //    A single public key in the public_key field.
                    //    A list of public keys in the public_keys field.
                    val thirdPartyInviteContentTry = for {
                      eventContent <- eventRepo.getEventContent(thirdPartyInviteEvent.eventId)
                      content      <- eventContent.get.content.get.as[ThirdPartyInviteContent].toTry
                    } yield content
                    thirdPartyInviteContentTry
                      .map { thirdPartyInviteContent =>
                        val signaturesKey: Seq[String] = content.third_party_invite.get.signed.signatures.keys.toSeq
                        val thirdPartySignaturesKey: Seq[String] = thirdPartyInviteContent.public_keys
                          .getOrElse(List.empty)
                          .map(_.public_key)
                          .appended(thirdPartyInviteContent.public_key)
                        if (thirdPartySignaturesKey.intersect(signaturesKey).isEmpty)
                          Left(SignatureNotFound(event))
                        else Right(event)
                      }
                      .recoverWith {
                        case f =>
                          Success(
                            Left(
                              InternalError(
                                event,
                                s"third party event  ${thirdPartyInviteEvent.mxEventId} content couldn't be decoded to ThirdPartyInviteContent: ${f.getMessage}"
                              )
                            )
                          )
                      }
                      .get
                  }
                }
                .getOrElse(
                  Left(NoThirdPartyInviteEvent(event))
                ) //If there is no m.room.third_party_invite event in the current room state with state_key matching token, reject.
            }
        }
      }
    }

  private def validateJoinMemberShip(
      event: Event,
      stateSet: StateSet
  ): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validatePreviousEvent(event)
    val f2 = validateSender(event, stateSet)
    for {
      _ <- f1
      _ <- f2
    } yield event
  }

  /**
    * If the only previous event is an m.room.create and the state_key is the creator, allow.
    * @param event
    * @return
    */
  private def validatePreviousEvent(event: Event): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future
        .fromTry(parentChain(event))
        .map { parents =>
          if (parents.size == 1 && parents.head.eventType != EventTypes.M_ROOM_CREATE)
            Left(ParentEventMustBeUniqueAndCreate(event))
          else Right(event)

        }
        .recover(f => Left(InternalError(event, s"Couldn't get event's parents: ${f.getMessage}")))
    }

  private def validateSender(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future
        .fromTry(authRepo.getUserByIdWithCache(event.senderId))
        .flatMap { sender =>
          (sender, event.stateKey) match {
            case (Some(sender), Some(stateKey)) if sender.mxUserId == stateKey =>
              Future
                .fromTry(hasUserBannedEvent(sender.mxUserId, stateSet))
                .map {
                  case Some(banEvent) => Left(SenderIsBanned(event, banEvent))
                  case None           => Right(event)
                }
            case _ => Future.successful(Left(SenderMismatchStatekey(event)))
          }
        }
        .recover(f =>
          Left(InternalError(event, s"Couldn't not get event sender or ban event (if it exists): ${f.getMessage}"))
        )
    }

  private def validateLeaveMemberShip(
      event: Event,
      stateSet: StateSet
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val plFuture                   = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val targetUserMembershipFuture = Future.fromTry(currentMembership(event.stateKey.get, stateSet))
      val targetPLFuture             = Future.fromTry(stateSetService.userPowerLevel(event.stateKey.get, stateSet))
      val senderFuture               = Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
      val senderMembershipFuture =
        senderFuture.flatMap(sender => Future.fromTry(currentMembership(sender.get.mxUserId, stateSet)))
      val senderPLFuture =
        senderFuture.flatMap(sender => Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet)))

      val memberShipFuture = for {
        sender           <- senderFuture
        senderMembership <- senderMembershipFuture
        senderPL         <- senderPLFuture
        targetMembership <- targetUserMembershipFuture
        targetPL         <- targetPLFuture
        pl               <- plFuture
      } yield (sender, senderMembership, senderPL, targetMembership, targetPL, pl)
      memberShipFuture
        .map {
          case (
                Some(sender),
                Some((_, senderMembership)),
                Some(senderPl),
                Some((_, targetMembership)),
                Some(targetPl),
                Some(pl)
              ) =>
            if (
              sender.mxUserId == event.stateKey.get && (senderMembership.membership != "invite" || senderMembership.membership != "join")
            ) {
              Left(IncompatibleMembership(event, sender.mxUserId, senderMembership.membership))
            } else if (senderMembership.membership != "join") {
              Left(IncompatibleMembership(event, sender.mxUserId, senderMembership.membership))
            } else if (targetMembership.membership == "ban" && senderPl < pl.ban) {
              Left(InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban))
            } else if (senderPl >= pl.kick && targetPl < senderPl) {
              Right(event)
            } else Left(InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban))
          case _ => Left(InternalError(event, "Couldn't validate leave membership sender"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't not validate membership event: ${f.getMessage}")))
    }

  private def validateBanMemberShip(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val plFuture       = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val targetPLFuture = Future.fromTry(stateSetService.userPowerLevel(event.stateKey.get, stateSet))
      val senderFuture   = Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
      val senderMembershipFuture =
        senderFuture.flatMap(sender => Future.fromTry(currentMembership(sender.get.mxUserId, stateSet)))
      val senderPLFuture =
        senderFuture.flatMap(sender => Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet)))

      val memberShipFuture = for {
        sender           <- senderFuture
        senderMembership <- senderMembershipFuture
        senderPL         <- senderPLFuture
        targetPL         <- targetPLFuture
        pl               <- plFuture
      } yield (sender, senderMembership, senderPL, targetPL, pl)
      memberShipFuture
        .map {
          case (
                Some(sender),
                Some((_, senderMembership)),
                Some(senderPl),
                Some(targetPl),
                Some(pl)
              ) =>
            if (senderMembership.membership != "join") {
              Left(IncompatibleMembership(event, sender.mxUserId, senderMembership.membership))
            } else if (senderPl >= pl.ban && targetPl < senderPl) {
              Right(event)
            } else Left(InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban))
          case _ => Left(InternalError(event, "Couldn't validate ban membership sender"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't not validate membership event: ${f.getMessage}")))
    }

  def validate(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)

    val eventContentFuture: Future[MemberEventContent] = for {
      eventContent <- Future.fromTry(eventRepo.getEventContent(event.eventId))
      content      <- Future.fromTry(eventContent.get.content.get.as[MemberEventContent].toTry)
    } yield content

    val f2 = EitherT.right(eventContentFuture).flatMap { membershipContent =>
      membershipContent.membership match {
        case "join"   => validateJoinMemberShip(event, stateSet)
        case "invite" => validateInviteMemberShip(event, stateSet, membershipContent)
        case "leave"  => validateLeaveMemberShip(event, stateSet)
        case "ban"    => validateBanMemberShip(event, stateSet)
        case other =>
          EitherT[Future, EventAuthValidation, Event](Future.successful(Left(UnknownMembership(event, other))))
      }
    }

    for {
      _ <- f1
      _ <- f2
    } yield event
  }
}
