/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import java.util.UUID
import io.circe.generic.auto._
import io.circe.parser.decode
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Account, Filter}
import org.mascarene.homeserver.internal.repository.FilteringRepo
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition
import org.mascarene.utils.Codecs

import scala.util.Try

class FilteringService(implicit runtimeContext: RuntimeContext) {
  private val filteringRepo = new FilteringRepo

  def createFilter(account: Account, filterDefinition: FilterDefinition): Try[Filter] =
    filteringRepo.createFilter(account.accountId, filterDefinition)

  def getFilter(filterId: UUID): Try[Option[Filter]] = filteringRepo.getFilter(filterId)

  def parseOrGetFilterDefinition(filterParameter: String): Try[FilterDefinition] = {
    if (filterParameter.startsWith("{")) {
      decode[FilterDefinition](filterParameter).toTry
    } else {
      getFilter(UUID.fromString(Codecs.base64Decode(filterParameter)))
        .map(_.get.filterDefinition.getOrElse(FilterDefinition()))
    }
  }

}
