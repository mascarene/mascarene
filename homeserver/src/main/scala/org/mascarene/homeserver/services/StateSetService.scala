/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import com.typesafe.scalalogging.LazyLogging
import io.circe._
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, EventContent, EventTypes, Room, StateSet}
import org.mascarene.homeserver.internal.repository.{EventRepo, StateSetVersionRepo}
import org.mascarene.homeserver.internal.rooms.PowerLevelsUtils
import org.mascarene.matrix.client.r0.model.ModelEventUtils
import org.mascarene.matrix.client.r0.model.events.{
  MemberEventContent,
  RoomAvatarEventContent,
  RoomCanonicalAliasEventContent,
  RoomGuestAccessEventContent,
  RoomHistoryVisibilityEventContent,
  RoomNameEventContent,
  RoomTopicEventContent,
  StateEvent
}
import org.mascarene.matrix.client.r0.model.rooms.PowerLevelEventContent

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Success, Try}

/**
  * API methods to manipulate a state set or using its content
  */
class StateSetService(implicit val runtimeContext: RuntimeContext) extends LazyLogging {
  private[this] val eventRepo           = new EventRepo
  private[this] val stateSetVersionRepo = new StateSetVersionRepo
  private[this] val eventService        = runtimeContext.newEventService

  /**
    * Load the m.room.power_levels event content of the given state set
    * @param stateSet
    * @return
    */
  def getPowerLevelEventContent(stateSet: StateSet): Try[Option[PowerLevelEventContent]] =
    extractStateEvent(stateSet, EventTypes.M_ROOM_POWER_LEVELS, "").map {
      case None                          => None
      case Some((_, Some(eventContent))) => Some(PowerLevelsUtils.getPlContent(eventContent))
    }

  private def extractStateEvent(
      stateSet: StateSet,
      eventType: String,
      stateKey: String
  ): Try[Option[(Event, Option[EventContent])]] = {
    stateSet.getEvent(eventType, stateKey) match {
      case None => Success(None)
      case Some(stateEvent: Event) =>
        eventRepo.getEventContent(stateEvent.eventId).map(eventContent => Some(stateEvent, eventContent))
    }
  }

  def getCanonicalAlias(stateSet: StateSet): Try[Option[RoomCanonicalAliasEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_CANONICAL_ALIAS, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomCanonicalAliasEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  def getRoomName(stateSet: StateSet): Try[Option[RoomNameEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_NAME, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomNameEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  def getRoomTopic(stateSet: StateSet): Try[Option[RoomTopicEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_TOPIC, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomTopicEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  def getRoomHistoryVisibility(stateSet: StateSet): Try[Option[RoomHistoryVisibilityEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_HISTORY_VISIBILITY, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomHistoryVisibilityEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  def getRoomGuestAccess(stateSet: StateSet): Try[Option[RoomGuestAccessEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_GUEST_ACCESS, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomGuestAccessEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  def getRoomAvatar(stateSet: StateSet): Try[Option[RoomAvatarEventContent]] = {
    for {
      stateEventWithContent <- extractStateEvent(stateSet, EventTypes.M_ROOM_AVATAR, "")
      eventContent          <- Try { stateEventWithContent.flatMap(_._2) }
      canonicalContent <- eventContent match {
        case Some(ec) => ec.content.get.as[RoomAvatarEventContent].toTry.map(Some(_))
        case None     => Success(None)
      }
    } yield canonicalContent
  }

  /**
    * Calculate the user power level from power level event
    * @param userMxId user to find power level for
    * @param stateSet room state at event
    * @return a may be integer
    */
  def userPowerLevel(userMxId: String, stateSet: StateSet): Try[Option[Int]] = {
    getPowerLevelEventContent(stateSet)
      .map(mayBePlContent => mayBePlContent.map(PowerLevelsUtils.findUserPowerLevel(userMxId, _)))
  }

  /**
    * Get the m.room.member event with user state key from the given state set
    * @param userMxId user matrix Id to used as state key to look for m.room.member event in statekey
    * @param stateSet state key to look for
    * @return
    */
  def getMembershipEventContent(userMxId: String, stateSet: StateSet): Try[Option[MemberEventContent]] = {
    extractStateEvent(stateSet, EventTypes.M_ROOM_MEMBER, userMxId).flatMap {
      case None => Success(None)
      case Some((_, Some(eventContent))) =>
        eventContent.content match {
          case None     => Success(None)
          case Some(ec) => ModelEventUtils.decodeMemberEventContent(ec).map(Some(_))
        }
    }
  }

  /**
    * Get a m.room.member event content
    * @param memberEvent event to get gontent
    * @return
    */
  def getMembershipEventContent(memberEvent: Event): Try[MemberEventContent] = {
    for {
      eventContent <- eventRepo.getEventContent(memberEvent.eventId)
      content      <- ModelEventUtils.decodeMemberEventContent(eventContent.get.content.get)
    } yield content
  }

  /**
    * Store a new state version (contained in `newStateSet`) for the give `room`.
    * @param room room associated this the state set
    * @param newStateSet new state set to store
    * @param previousStateSet previous state set (needed for calculating changes diff)
    * @return
    */
  def createNewVersion(room: Room, newStateSet: StateSet, previousStateSet: StateSet): Try[Long] = {
    val diffStateSet = newStateSet.diff(previousStateSet)
    val diffEvents   = diffStateSet.map { (_, _, event) => event.eventId }.toSet
    stateSetVersionRepo.createVersion(room.roomId, diffEvents)
  }

  /**
    * Load a room stateSet version at the given event
    * @param event
    * @return
    */
  def loadStateSetForEvent(event: Event): Try[StateSet] = {
    for {
      stateEventsIds <- stateSetVersionRepo.getVersionStateEvents(event.roomId, event.statesetVersion.getOrElse(-1))
      stateEvents    <- eventRepo.getEventsByIds(stateEventsIds)
    } yield StateSet.fromIterable(stateEvents.toList.sortBy(_.streamOrder).map(e => (e.eventType, e.stateKey.get, e)))
  }

  def loadStateSetForEventWithCache(event: Event): Try[StateSet] =
    Try {
      runtimeContext.eventStateSetCache
        .getIfPresent(event.eventId)
        .orElse {
          loadStateSetForEvent(event).map { stateSet =>
            runtimeContext.eventStateSetCache.put(event.eventId, stateSet)
            Some(stateSet)
          }.get
        }
        .get
    }

  /**
    * Load the room stateSet at the stream order
    * @param event
    * @return
    */
  def loadStateSetAtStreamOrder(streamOrder: Long): Try[Option[StateSet]] = {
    eventRepo.getEventAtStreamOrder(streamOrder).flatMap {
      case None => Success(None)
      case Some(event) =>
        val tryStateSet = for {
          stateEventsIds <- stateSetVersionRepo.getVersionStateEvents(event.roomId, event.statesetVersion.getOrElse(-1))
          stateEvents    <- eventRepo.getEventsByIds(stateEventsIds)
        } yield StateSet.fromIterable(stateEvents.map(e => (e.eventType, e.stateKey.get, e)))
        tryStateSet.map(Some(_))
    }
  }

  def stateSetToApiModel(stateSet: StateSet, roomMxId: Option[String] = None): Future[List[StateEvent]] = {
    val eventList: List[Event] = stateSet.toList.map(_._3)
    val contentsFuture         = Future.fromTry(eventService.getEventsContents(eventList))
    val sendersFuture          = Future.fromTry(eventService.getEventsSenders(eventList))
    val apiFuture = for {
      contents <- contentsFuture
      senders  <- sendersFuture
    } yield (contents, senders)

    apiFuture.map {
      case (contents, senders) =>
        eventList
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            org.mascarene.matrix.client.r0.model.events.StateEvent(
              content.content.getOrElse(Json.Null),
              event.eventType,
              event.mxEventId,
              sender.mxUserId,
              event.originServerTs.toEpochMilli,
              event.unsigned,
              event.stateKey.get,
              None,
              roomMxId
            )
          }
    }
  }

}
