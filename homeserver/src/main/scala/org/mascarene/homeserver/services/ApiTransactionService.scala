/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import java.util.UUID
import com.typesafe.scalalogging.LazyLogging
import io.circe.Json
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{ApiTransaction, AuthToken, Event, EventTransaction}
import org.mascarene.homeserver.internal.repository.{ApiTransactionRepo, EventRepo}

import scala.util.{Failure, Success, Try}

class ApiTransactionService(implicit val runtimeContext: RuntimeContext) extends LazyLogging with ImplicitAskTimeOut {
  private val apiTransactionRepo = new ApiTransactionRepo
  private val eventRepo          = new EventRepo

  def storeApiTransaction(path: String, txnId: String, authToken: AuthToken, content: Json): Try[ApiTransaction] =
    apiTransactionRepo.storeApiTransaction(path, txnId, authToken.tokenId, content)
  def storeApiTransaction(
      path: String,
      txnId: String,
      authToken: AuthToken,
      event: Event,
      content: Json
  ): Try[(ApiTransaction, EventTransaction)] =
    apiTransactionRepo.storeApiTransaction(path, txnId, authToken.tokenId, content).map { apiTransaction =>
      eventRepo.insertEventTransaction(event.eventId, apiTransaction.transactionId) match {
        case Success(eventTransaction) => (apiTransaction, eventTransaction)
        case Failure(f)                => throw f
      }
    }

  def getApiTransaction(txnId: String): Try[Option[ApiTransaction]] = apiTransactionRepo.getApiTransaction(txnId)

  def getApiTransactionWithAuthToken(transactionId: UUID): Try[Option[(ApiTransaction, AuthToken)]] =
    apiTransactionRepo.getApiTransactionWithAuthToken(transactionId)

}
