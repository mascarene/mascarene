/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.auth

import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Directives._
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.AuthCredentials
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.ApiErrorRejection
import org.mascarene.homeserver.services.AuthService
import org.mascarene.sdk.matrix.core.{ApiError, ApiErrors, ApiFailure}
import org.uaparser.scala.{Client, Parser}

import scala.util.{Failure, Success}

trait AuthDirectives extends ImplicitAskTimeOut {
  protected def authService: AuthService

  /**
    * extract auth token from `access_token` query parameter or Authorization HTTP Header
    */
  private val extractAuthToken: Directive1[Option[String]] = parameter("access_token".?).flatMap {
    case Some(token) => provide(Some(token))
    case _           => optionalHeaderValueByName("Authorization")
  }

  /**
    * Code auth token to existing credentials
    * @return
    */
  private val bearerRegex = """^Bearer\s+(.*)""".r
  private def decodeAuthToken(encodedToken: String): Directive1[AuthCredentials] = {
    val token = encodedToken match {
      case bearerRegex(token) => token
      case _                  => ""
    }
    authService.decodeAuthToken(token) match {
      case Success(credentials) => provide(credentials)
      case Failure(failure: ApiFailure) =>
        reject(ApiErrorRejection(failure.toApiError))
      case Failure(failure) =>
        reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
    }
  }

  def requireAuth: Directive1[AuthCredentials] =
    extractAuthToken.flatMap {
      case Some(token) => decodeAuthToken(token)
      case None        => reject(server.ApiErrorRejection(ApiError("M_MISSING_TOKEN", Some("Missing access token"))))
    }

  def extractUserAgent: Directive1[Client] =
    optionalHeaderValueByName("User-Agent").flatMap { header =>
      provide(Parser.default.parse(header.getOrElse("")))
    }

}
