/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.homeserver.services.AuthService
import org.mascarene.matrix.client.r0.model.{
  Capabilities,
  CapabilitiesResponse,
  ChangePasswordCapability,
  RoomVersionsCapability
}

import scala.jdk.CollectionConverters._
import scala.jdk.DurationConverters._

final case class Version(versions: Seq[String], unstable_features: Option[Map[String, Boolean]] = None)

class ClientApiRoutes(system: ActorSystem[Nothing], clientApiRateLimiter: RateLimiter)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with ThrottleDirective
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala

  protected val authService: AuthService = runtimeContext.newAuthService

  private val baseUrl            = runtimeContext.config.getString("mascarene.server.base-url")
  private val defaultRoomVersion = runtimeContext.config.getString("mascarene.matrix.default-room-version")
  private val supportedRoomVersions =
    runtimeContext.config.getStringList("mascarene.matrix.supported-room-versions").asScala.toList
  private val changePasswordCapability =
    runtimeContext.config.getBoolean("mascarene.matrix.capabilities.change-password")

  lazy val routes: Route = pathPrefix("_matrix" / "client") {
    path("versions") {
      get {
        complete(Version(Seq("r0.6.0")))
      }
    } ~ path("r0" / "capabilities") {
      withThrottle(clientApiRateLimiter, throttleWaitTime) {
        requireAuth { _ =>
          get {
            logger.info(s"API CALL - GET capabilities")
            complete(
              CapabilitiesResponse(
                Capabilities(
                  ChangePasswordCapability(changePasswordCapability),
                  RoomVersionsCapability(
                    defaultRoomVersion,
                    supportedRoomVersions.map(version => version -> "stable").toMap
                  )
                )
              )
            )
          }
        }
      }
    }
  } ~ path(".well-known" / "matrix" / "client") {
    get {
      complete(Map("m.homeserver" -> Map("base_url" -> baseUrl)))
    }
  }
}
