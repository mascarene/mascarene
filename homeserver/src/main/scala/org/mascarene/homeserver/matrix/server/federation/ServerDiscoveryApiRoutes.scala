/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.federation

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.version.BuildInfo
import org.mascarene.matrix.server.r0.model.{Server, ServerVersionResponse, WellKnownResponse}

import scala.util.Try

class ServerDiscoveryApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val delegateServer: Option[String] = Try {
    runtimeContext.config.getString("mascarene.server.federation.delegate-server")
  }.toOption

  lazy val routes: Route = path(".well-known" / "matrix" / "server") {
    get {
      delegateServer match {
        case None           => complete(StatusCodes.NotFound)
        case Some(delegate) => complete(WellKnownResponse(delegate))
      }
    }
  } ~
    path("_matrix" / "federation" / "v1" / "version") {
      get {
        complete(ServerVersionResponse(Server("Mascarene", BuildInfo.version)))
      }
    }
}
