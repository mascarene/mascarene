package org.mascarene.homeserver.matrix.server.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives.{complete, get, pathPrefix}
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import akka.http.scaladsl.server.Directives._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.matrix.client.r0.model.thirdparty.Protocol

class ThirdPartyNetworksApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with ImplicitAskTimeOut {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "thirdparty") {
    path("protocols") {
      //GET /_matrix/client/r0/publicRooms
      get {
        val protocols = Map.empty[String, Protocol]
        complete(protocols)
      }
    }
  }
}
