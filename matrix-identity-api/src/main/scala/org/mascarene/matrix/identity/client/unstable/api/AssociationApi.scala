/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.identity.client.unstable.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import org.mascarene.matrix.identity.model.association.LookupResponse
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContextExecutor, Future}

trait AssociationApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def lookup(medium: String, address: String): Future[LookupResponse] =
    doGet[LookupResponse](
      apiRoot.withPath(apiRoot.path + "/api/v1/lookup"),
      Seq.empty,
      Map("medium" -> medium, "address" -> address)
    ).map(_.entity)

}
