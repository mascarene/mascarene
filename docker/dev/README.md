## Mascarene configuration for development

This samples provides prerequisites for running mascarene in developer environment. 
It provides:
 - a PostgreSQL database
 - a Redis database
 - a Nginx reverse proxy for exposing Riot and mascarene through HTTPS

### Configuration

#### Database

Development requires two PostgreSQL database.
 1. one for storing Mascarene data
 2. one for storing Synapse test instance
 
Before runnning Mascarene and Synapse, you need to create the appropriate PostgreSQL database:

```

$ docker-compose exec db psql -U postgres postgres
postgres=# CREATE USER mascarene WITH ENCRYPTED PASSWORD 'some_password';
postgres=# CREATE DATABASE mascarene OWNER mascarene;
postgres=# CREATE USER synapse WITH ENCRYPTED PASSWORD 'synapse';
postgres=# CREATE DATABASE synapse ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse;
```

#### TLS certificates

Both synapse and mascarene requires TLS secured connection, so you will need valid certificates for running both servers.
Because auto-signed certificates can be painful, the suggested solution is to use [mkcert](https://github.com/FiloSottile/mkcert) to get locally trusted certificates.

Once you have installed mkcert, use the following commands to install the RootCA and create two certificates:

```
mkcert -install
cd ssl_config
mkcert mascarene.dev.mascarene.org
cd -
cd synapse
mkcert synapse.dev.mascarene.org
```

Synapse and nginx reverse proxy are already configures to use the files.

Finally, add the following line to you `/etc/hosts` file:

```
127.0.0.1  synapse.dev.mascarene.org
127.0.0.1  mascarene.dev.mascarene.org
```


#### Mascarene 
See `mascarene.conf`

### Running

```
docker-compose up -d
```

Then run `org.mascarene.homeserver.MascareneMain` class with VM argument `-Dconfig.file=./docker/dev/mascarene.conf` 

