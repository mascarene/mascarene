package org.mascarene.matrix.client.r0.model

import io.circe.Json

package object thirdparty {
  case class Protocol(
      user_fields: List[String],
      location_fields: List[String],
      icon: String,
      field_types: Map[String, FieldType],
      instances: List[ProtocolInstance]
  )
  case class FieldType(regexp: String, placeholder: String)
  case class ProtocolInstance(desc: String, icon: Option[String], fields: Json, network_id: String)
}
