package org.mascarene.matrix.client.r0.model

case class CapabilitiesResponse(capabilities: Capabilities)

case class Capabilities(`m.change_password`: ChangePasswordCapability, `m.room_versions`: RoomVersionsCapability)
case class ChangePasswordCapability(enabled: Boolean)
case class RoomVersionsCapability(default: String, available: Map[String, String])
