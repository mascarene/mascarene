/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.matrix.server.r0.model

case class ServerKeysResponse(
    server_name: String,
    verify_keys: Map[String, VerifyKey],
    old_verify_keys: Map[String, OldVerifyKey],
    signatures: Option[Map[String, Map[String, String]]] = None,
    valid_until_ts: Option[Long] = None
)
case class VerifyKey(key: String)
case class OldVerifyKey(expired_ts: Long, key: String)
