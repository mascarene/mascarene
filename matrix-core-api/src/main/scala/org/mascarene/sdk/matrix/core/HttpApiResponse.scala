package org.mascarene.sdk.matrix.core

import akka.http.scaladsl.model.HttpHeader

case class HttpApiResponse[R](entity: R, headers: Seq[HttpHeader])
