/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.sdk.matrix.core

class ApiFailure(errCode: String, message: String = "", error: Option[String] = None) extends Exception(message) {
  def this(errCode: String, message: String, error: Option[String], cause: Throwable) = {
    this(message, errCode, error)
    initCause(cause)
  }

  def toApiError: ApiError = ApiError(errCode, error.orElse(Some(message)))
}
