/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model._
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.Transport
import org.mascarene.matrix.client.r0.model.Versions
import org.mascarene.matrix.client.r0.model.auth.AuthToken

import scala.concurrent.Future

trait BaseApi { self: Transport =>
  implicit def actorSystem: ActorSystem[Nothing]
  implicit private val ec = actorSystem.executionContext

  protected def apiRoot: Uri

  /**
    * Call to GET /_matrix/client/versions
    * @return Versions object
    */
  def serverVersion(implicit token: Option[AuthToken] = None): Future[Versions] =
    doGet[Versions](apiRoot.withPath(apiRoot.path + "/versions")).map(_.entity)
}
