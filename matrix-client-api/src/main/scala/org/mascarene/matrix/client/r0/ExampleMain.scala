/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.Uri
import com.typesafe.scalalogging.LazyLogging
import io.circe.{Decoder, Encoder}
import org.mascarene.matrix.client.r0.api.{AccountApi, AuthApi, BaseApi, EventsApi}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object ExampleMain extends App with LazyLogging {
  val _system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "ExampleMain") //an ActorSystem

  //val client = new MatrixClient("http://localhost:18008")
  val client = new BaseApi
    with AuthApi
    with AccountApi
    with EventsApi
    with HttpTransport
    with ClientResponseDecodeHandler {
    override implicit def actorSystem = _system

    override protected def apiRoot: Uri = Uri("http://localhost:18008").withPath(Uri.Path("/_matrix/client"))

  }
  /*
  client.serverVersion().onComplete {
    case Success(versions) =>
      logger.info(versions.toString)
    //system.terminate()
    case Failure(e) =>
      sys.error("something wrong: " + e)
      _system.terminate()
  }

  client
    .login(LoginRequest("m.login.password", user = Some("nico"), password = Some("nicolas")))
    .flatMap { res =>
      client.whoAmI(res.access_token).flatMap(whoamI => client.logout(res.access_token))
    }
  client
    .register("user", RegisterRequest(Map.empty, false, "user54321", "password", "device1", "My Device"))
    .onComplete {
      case Failure(t: AuthFlowException) =>
        client
          .register(
            "user",
            RegisterRequest(
              Map("type"     -> "m.login.dummy",
                  "session"  -> t.session.get,
                  "response" -> "CAPTCHA",
                  "user"     -> "user12345TT",
                  "password" -> "1password"),
              false,
              "user12345TT",
              "nicolas",
              "device1",
              "My Device"
            )
          )
          .onComplete {
            case r => println(s"!! $r !!")
          }
      case Failure(e) => sys.error("something wrong" + e)
    }
  client
    .login(LoginRequest("m.login.password", user = Some("toto"), password = Some("nicolas")))
    .flatMap { res =>
      implicit val token = AuthToken(res.access_token)
      client
        .sync(timeout = Some(30000))
        .map(response => logger.info(s"$response"))
    }
   */
}
