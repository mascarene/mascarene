Mascarene is an open source *homeserver* implementation of the [Matrix protocol](https://matrix.org/docs/spec/).

[![Liberapay](http://img.shields.io/liberapay/receives/Nico.svg?logo=liberapay)](https://liberapay.com/Nico/)
[![Matrix](https://img.shields.io/matrix/mascarene:beerfactory.org?server_fqdn=matrix.beerfactory.org)](https://matrix.to/#/#mascarene:beerfactory.org)


[Matrix](https://matrix.org/) is an open standard for interoperable, decentralised, real-time communication over IP.
It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication -
or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the
conversation history.

**THIS PROJECT IS STILL UNDER HEAVY CONSTRUCTION**

# Features

*This sections describes features planned to be implemented, not features currently available.
See [CHANGELOG](CHANGELOG.md) for current features and implementation status.*

Mascarene implements server side of current [Matrix API specifications](https://matrix.org/docs/spec/) :
 - [client-server API](https://matrix.org/docs/spec/client_server/r0.6.0) (0.6.0)
 - [server-server API](https://matrix.org/docs/spec/server_server/r0.1.4) (0.1.4)
 - [application service API](https://matrix.org/docs/spec/application_service/unstable.html) (unstable)
 - [identity service API](https://matrix.org/docs/spec/identity_service/unstable.html) (unstable)
 - [push gateway API](https://matrix.org/docs/spec/push_gateway/unstable.html) (unstable)
